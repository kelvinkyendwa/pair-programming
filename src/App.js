import React, { useState, useMemo } from "react";
import Home from "./pages/Home";
import About from "./pages/About";
import "./styles.css";
import { Router, Link, Switch, Route } from "react-router-dom";

import { ContextLogin } from "./context";

export default function App() {
  const [authKey, setAuthKey] = useState('');

  const value = useMemo(
    () => ({
      authKey,
      setAuthKey
    }),
    [authKey]
  );

  return (

      <Router>
        <Link to="/home">Home</Link>
        <Link to="/about">About</Link>

        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>

  );
}
/*
const login_service(email, password){
  getKey();
  return getKey();
}

handleLogin(){
  login_service(email, password)
  //state
}
*/
